package ru.pisarev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IBusinessRepository;
import ru.pisarev.tm.api.IBusinessService;
import ru.pisarev.tm.api.service.IConnectionService;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    @NotNull
    public AbstractBusinessService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    public abstract IBusinessRepository<E> getRepository(@NotNull Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            return repository.findAll(userId);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull final String userId, @Nullable final Collection<E> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            repository.addAll(userId, collection);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E add(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) return null;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            @Nullable final E entityResult = repository.add(userId, entity);
            connection.commit();
            return entityResult;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            return repository.findById(userId, optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            repository.removeById(userId, optionalId.orElseThrow(EmptyIdException::new));
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) return;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            repository.remove(userId, entity);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
