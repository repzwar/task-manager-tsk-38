package ru.pisarev.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.other.ISaltSetting;
import ru.pisarev.tm.api.other.ISignatureSetting;

public interface IPropertyService extends ISaltSetting, ISignatureSetting {

    String getApplicationVersion();

    @NotNull String getServerHost();

    @NotNull String getServerPort();

    @Nullable String getJdbcUser();

    @Nullable String getJdbcPassword();

    @Nullable String getJdbcUrl();
}
