package ru.pisarev.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(final String login);

    User findByEmail(final String email);

    void removeUserByLogin(final String login);

    @Nullable
    @SneakyThrows
    User add(@Nullable User entity);

    @Nullable
    @SneakyThrows
    User update(@Nullable User entity);
}
