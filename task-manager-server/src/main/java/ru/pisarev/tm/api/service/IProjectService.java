package ru.pisarev.tm.api.service;

import ru.pisarev.tm.api.IBusinessService;
import ru.pisarev.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final Integer index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final Integer index);

    Project updateById(final String userId, final String id, final String name, final String description);

    Project updateByIndex(final String userId, final Integer index, final String name, final String description);

    Project startById(final String userId, final String id);

    Project startByIndex(final String userId, final Integer index);

    Project startByName(final String userId, final String name);

    Project finishById(final String userId, final String id);

    Project finishByIndex(final String userId, final Integer index);

    Project finishByName(final String userId, final String name);

    Project add(String userId, String name, String description);
}
