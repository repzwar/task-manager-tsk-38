package ru.pisarev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.constant.FieldConst;
import ru.pisarev.tm.constant.TableConst;
import ru.pisarev.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    protected String getTableName() {
        return TableConst.PROJECT_TABLE;
    }

    @Override
    @SneakyThrows
    protected Project fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setId(row.getString(FieldConst.ID));
        project.setUserId(row.getString(FieldConst.USER_ID));
        project.setStartDate(row.getDate(FieldConst.START_DATE));
        project.setFinishDate(row.getDate(FieldConst.FINISH_DATE));
        project.setCreated(row.getDate(FieldConst.CREATED));
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@Nullable final Project entity) {
        if (entity == null) return null;
        @NotNull final String query = "INSERT INTO " + getTableName() +
                "(id, name, description, status, start_date, finish_date, created, user_id)" +
                "VALUES(?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setDate(5, prepare(entity.getStartDate()));
        statement.setDate(6, prepare(entity.getFinishDate()));
        statement.setDate(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project update(@Nullable final Project entity) {
        if (entity == null) return null;
        @NotNull final String query = "UPDATE " + getTableName() +
                "SET name=?, description=?, status=?, start_date=?, finish_date=?, created=?, user_id=? WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getName());
        statement.setString(2, entity.getDescription());
        statement.setString(3, entity.getStatus().toString());
        statement.setDate(4, prepare(entity.getStartDate()));
        statement.setDate(5, prepare(entity.getFinishDate()));
        statement.setDate(6, prepare(entity.getCreated()));
        statement.setString(7, entity.getUserId());
        statement.setString(8, entity.getId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findByName(@NotNull final String userId, @Nullable final String name) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE name = ? AND user_id=? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final Project result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findByIndex(@NotNull final String userId, final int index) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id=? LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(2, index - 1);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final Project result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE name = ? AND user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, final int index) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @Nullable
    @SneakyThrows
    public Project add(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (name == null) return null;
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

}
