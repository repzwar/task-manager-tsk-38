package ru.pisarev.tm.api.service;

import lombok.SneakyThrows;

import java.sql.Connection;

public interface IConnectionService {
    @SneakyThrows
    Connection getConnection();
}
