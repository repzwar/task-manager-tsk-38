package ru.pisarev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.repository.ISessionRepository;
import ru.pisarev.tm.api.service.IConnectionService;
import ru.pisarev.tm.api.service.IUserService;
import ru.pisarev.tm.enumerated.Role;
import ru.pisarev.tm.exception.system.AccessDeniedException;
import ru.pisarev.tm.model.Session;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.repository.SessionRepository;
import ru.pisarev.tm.util.HashUtil;

import java.sql.Connection;
import java.util.List;

public final class SessionService extends AbstractService<Session> implements ru.pisarev.tm.api.service.ISessionService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public SessionService(
            @NotNull final IConnectionService connectionService,
            @NotNull IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.userService = userService;
        this.propertyService = propertyService;
    }

    public ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

    @Override
    @SneakyThrows
    @Nullable
    public Session open(@Nullable final String login, @Nullable final String password) {
        @Nullable final User user = checkDataAccess(login, password);
        if (user == null) throw new AccessDeniedException();
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = getRepository(connection);
            @Nullable final Session resultSession = sign(session);
            repository.add(session);
            connection.commit();
            return resultSession;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public User checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return null;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || hash.isEmpty() || user.isLocked()) return null;
        if (hash.equals(user.getPasswordHash())) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final Session session, final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = getRepository(connection);
            if (repository.findById(session.getId()) == null) throw new AccessDeniedException();
        } finally {
            connection.close();
        }

    }

    @Override
    @SneakyThrows
    @Nullable
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public void close(@Nullable final Session session) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = getRepository(connection);
            repository.remove(session);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void closeAllByUserId(@Nullable final String userId) {
        if (userId == null) return;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = getRepository(connection);
            repository.removeByUserId(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    @Nullable
    public List<Session> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = getRepository(connection);
            return repository.findAllByUserId(userId);
        } finally {
            connection.close();
        }
    }
}
