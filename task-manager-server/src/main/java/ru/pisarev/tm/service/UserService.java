package ru.pisarev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.repository.IUserRepository;
import ru.pisarev.tm.api.service.IConnectionService;
import ru.pisarev.tm.api.service.IUserService;
import ru.pisarev.tm.exception.empty.*;
import ru.pisarev.tm.exception.entity.EmailExistException;
import ru.pisarev.tm.exception.entity.LoginExistException;
import ru.pisarev.tm.exception.entity.UserNotFoundException;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.repository.UserRepository;
import ru.pisarev.tm.util.HashUtil;

import java.sql.Connection;
import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    public UserService(
            @NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    public IUserRepository getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            return userRepository.findByLogin(login);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            return userRepository.findByEmail(email);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.removeUserByLogin(login);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        @NotNull final User user = new User();
        user.setLogin(login);
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(
            @Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExist(email)) throw new EmailExistException(login);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }


    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }
}
