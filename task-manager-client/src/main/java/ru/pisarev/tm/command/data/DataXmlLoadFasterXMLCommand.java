package ru.pisarev.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.AuthAbstractCommand;

public class DataXmlLoadFasterXMLCommand extends AuthAbstractCommand {

    @Nullable

    public String name() {
        return "data-load-xml-f";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Load data from XML by FasterXML.";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().loadDataXml(getSession());
    }

}