package ru.pisarev.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-03-16T22:23:07.244+03:00
 * Generated source version: 3.4.2
 */
@WebService(targetNamespace = "http://endpoint.tm.pisarev.ru/", name = "AdminEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.pisarev.ru/AdminEndpoint/closeAllByUserIdRequest", output = "http://endpoint.tm.pisarev.ru/AdminEndpoint/closeAllByUserIdResponse")
    @RequestWrapper(localName = "closeAllByUserId", targetNamespace = "http://endpoint.tm.pisarev.ru/", className = "ru.pisarev.tm.endpoint.CloseAllByUserId")
    @ResponseWrapper(localName = "closeAllByUserIdResponse", targetNamespace = "http://endpoint.tm.pisarev.ru/", className = "ru.pisarev.tm.endpoint.CloseAllByUserIdResponse")
    public void closeAllByUserId(

            @WebParam(name = "session", targetNamespace = "")
                    ru.pisarev.tm.endpoint.Session session,
            @WebParam(name = "userId", targetNamespace = "")
                    java.lang.String userId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.pisarev.ru/AdminEndpoint/removeByLoginRequest", output = "http://endpoint.tm.pisarev.ru/AdminEndpoint/removeByLoginResponse")
    @RequestWrapper(localName = "removeByLogin", targetNamespace = "http://endpoint.tm.pisarev.ru/", className = "ru.pisarev.tm.endpoint.RemoveByLogin")
    @ResponseWrapper(localName = "removeByLoginResponse", targetNamespace = "http://endpoint.tm.pisarev.ru/", className = "ru.pisarev.tm.endpoint.RemoveByLoginResponse")
    public void removeByLogin(

            @WebParam(name = "session", targetNamespace = "")
                    ru.pisarev.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.pisarev.ru/AdminEndpoint/lockByLoginRequest", output = "http://endpoint.tm.pisarev.ru/AdminEndpoint/lockByLoginResponse")
    @RequestWrapper(localName = "lockByLogin", targetNamespace = "http://endpoint.tm.pisarev.ru/", className = "ru.pisarev.tm.endpoint.LockByLogin")
    @ResponseWrapper(localName = "lockByLoginResponse", targetNamespace = "http://endpoint.tm.pisarev.ru/", className = "ru.pisarev.tm.endpoint.LockByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.pisarev.tm.endpoint.User lockByLogin(

            @WebParam(name = "session", targetNamespace = "")
                    ru.pisarev.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.pisarev.ru/AdminEndpoint/findAllByUserIdRequest", output = "http://endpoint.tm.pisarev.ru/AdminEndpoint/findAllByUserIdResponse")
    @RequestWrapper(localName = "findAllByUserId", targetNamespace = "http://endpoint.tm.pisarev.ru/", className = "ru.pisarev.tm.endpoint.FindAllByUserId")
    @ResponseWrapper(localName = "findAllByUserIdResponse", targetNamespace = "http://endpoint.tm.pisarev.ru/", className = "ru.pisarev.tm.endpoint.FindAllByUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.pisarev.tm.endpoint.Session> findAllByUserId(

            @WebParam(name = "session", targetNamespace = "")
                    ru.pisarev.tm.endpoint.Session session,
            @WebParam(name = "userId", targetNamespace = "")
                    java.lang.String userId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.pisarev.ru/AdminEndpoint/unlockByLoginRequest", output = "http://endpoint.tm.pisarev.ru/AdminEndpoint/unlockByLoginResponse")
    @RequestWrapper(localName = "unlockByLogin", targetNamespace = "http://endpoint.tm.pisarev.ru/", className = "ru.pisarev.tm.endpoint.UnlockByLogin")
    @ResponseWrapper(localName = "unlockByLoginResponse", targetNamespace = "http://endpoint.tm.pisarev.ru/", className = "ru.pisarev.tm.endpoint.UnlockByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.pisarev.tm.endpoint.User unlockByLogin(

            @WebParam(name = "session", targetNamespace = "")
                    ru.pisarev.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );
}
