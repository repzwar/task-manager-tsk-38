package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.endpoint.Task;

import java.util.List;

public class TaskShowListCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        @Nullable List<Task> tasks = serviceLocator.getTaskEndpoint().findTaskAll(getSession());
        int index = 1;
        for (@NotNull Task project : tasks) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }
}
