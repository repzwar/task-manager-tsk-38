package ru.pisarev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pisarev.tm.marker.DBCategory;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.service.ConnectionService;
import ru.pisarev.tm.service.PropertyService;

import java.util.List;

public class TaskRepositoryTest {

    @Nullable
    private TaskRepository taskRepository;

    @Nullable
    private Task task;

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        taskRepository = new TaskRepository(connectionService.getConnection());
        task = taskRepository.add("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", new Task("Task"));
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Task", task.getName());

        @NotNull final Task taskById = taskRepository.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertTrue(tasks.size() > 1);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserId() {
        @NotNull final List<Task> tasks = taskRepository.findAll("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce");
        Assert.assertTrue(tasks.size() > 1);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Task> tasks = taskRepository.findAll("test");
        Assert.assertNotEquals(1, tasks.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final Task task = taskRepository.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Task task = taskRepository.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "34");
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdNull() {
        @NotNull final Task task = taskRepository.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrectUser() {
        @NotNull final Task task = taskRepository.findById("test", this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        taskRepository.removeById(task.getId());
        Assert.assertNull(taskRepository.findById(task.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        @NotNull final Task task = taskRepository.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "Task");
        Assert.assertNotNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Task task = taskRepository.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "34");
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameNull() {
        @NotNull final Task task = taskRepository.findByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrectUser() {
        @NotNull final Task task = taskRepository.findByName("test", this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        @NotNull final Task task = taskRepository.findByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", 1);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() {
        taskRepository.removeById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", task.getId());
        Assert.assertNull(taskRepository.findById(task.getId()));
    }


}